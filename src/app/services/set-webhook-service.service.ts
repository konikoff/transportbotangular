import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { FormGroup } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class TelegramServiceService {

  private readonly TELGRAM_API_URL = 'https://api.telegram.org/botYOURTOKEN/';

  constructor(private http: HttpClient) { }

  public setWebhook(setWebhookForm: FormGroup) {
    var httpParams = new HttpParams();
    httpParams = httpParams.append('url', setWebhookForm.controls['domainName'].value);
    console.log(httpParams);

    return this.http.get(this.TELGRAM_API_URL.replace('YOURTOKEN', setWebhookForm.controls['botToken'].value) + 'setWebhook', { params: httpParams });
  }

  public getWebhookInfo(setWebhookForm: FormGroup) {
    return this.http.get(this.TELGRAM_API_URL.replace('YOURTOKEN', setWebhookForm.controls['botToken'].value) + 'getWebhookInfo');
  }

  public deleteWebhook(setWebhookForm: FormGroup) {
    return this.http.get(this.TELGRAM_API_URL.replace('YOURTOKEN', setWebhookForm.controls['botToken'].value) + 'setWebhook');
  }
}
