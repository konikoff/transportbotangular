import { TestBed } from '@angular/core/testing';

import { TelegramServiceService } from './set-webhook-service.service';

describe('TelegramServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TelegramServiceService = TestBed.get(TelegramServiceService);
    expect(service).toBeTruthy();
  });
});
