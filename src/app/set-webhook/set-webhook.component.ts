import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { TelegramServiceService } from '../services/set-webhook-service.service';

@Component({
  selector: 'app-set-webhook',
  templateUrl: './set-webhook.component.html',
  styleUrls: ['./set-webhook.component.css']
})
export class SetWebhookComponent implements OnInit {

  public setWebhookForm = this.formBuilder.group({
    domainName: new FormControl(''),
    botToken: new FormControl(''),
    certificate: new FormControl('')
  });

  public setWebhookResponse: any;

  constructor(private formBuilder: FormBuilder,
    private telegramServiceService: TelegramServiceService) {

  }

  ngOnInit() {
  }

  setWebhook() {
    console.log('Doamin name: ' + this.setWebhookForm.controls['domainName'].value);

    this.telegramServiceService.setWebhook(this.setWebhookForm).subscribe(
      (response: Response) => {
        console.log("Response: " + JSON.stringify(response, null, 4));

        this.setWebhookResponse = JSON.stringify(response, null, 4);
      },
      (error: Response) => {
        console.log("Error: " + JSON.stringify(error, null, 4));
        this.setWebhookResponse = JSON.stringify(error, null, 4);
      }
    );
  }

  getWebhookInfo() {
    this.telegramServiceService.getWebhookInfo(this.setWebhookForm).subscribe(
      (response: Response) => {
        console.log("Response: " + JSON.stringify(response, null, 4));

        this.setWebhookResponse = JSON.stringify(response, null, 4);
      },
      (error: Response) => {
        console.log("Error: " + JSON.stringify(error, null, 4));
        this.setWebhookResponse = JSON.stringify(error, null, 4);
      }
    );
  }

  deleteWebhook() {
    this.telegramServiceService.deleteWebhook(this.setWebhookForm).subscribe(
      (response: Response) => {
        console.log("Response: " + JSON.stringify(response, null, 4));

        this.setWebhookResponse = JSON.stringify(response, null, 4);
      },
      (error: Response) => {
        console.log("Error: " + JSON.stringify(error, null, 4));
        this.setWebhookResponse = JSON.stringify(error, null, 4);
      }
    );
  }

}
