import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SetWebhookComponent } from './set-webhook.component';

describe('SetWebhookComponent', () => {
  let component: SetWebhookComponent;
  let fixture: ComponentFixture<SetWebhookComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SetWebhookComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SetWebhookComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
